-- MySQL dump 10.13  Distrib 5.7.33, for Win64 (x86_64)
--
-- Host: localhost    Database: ewebapp
-- ------------------------------------------------------
-- Server version	5.7.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `blogs`
--

DROP TABLE IF EXISTS `blogs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `blogs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `title` text,
  `avatar_path` text,
  `short_description` text,
  `content` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `blogs`
--

LOCK TABLES `blogs` WRITE;
/*!40000 ALTER TABLE `blogs` DISABLE KEYS */;
/*!40000 ALTER TABLE `blogs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar_path` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'Iphone','/storage/v1/admin-page/images/categories/iphone/avatar/3dta5vHdJFpECjb85Tqn.jpg',NULL,'2023-02-07 18:01:28','2023-02-07 18:30:08');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `order_detail`
--

DROP TABLE IF EXISTS `order_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `order_detail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `price` double DEFAULT NULL,
  `quantity` int(11) DEFAULT NULL,
  `total` double DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `order_detail`
--

LOCK TABLES `order_detail` WRITE;
/*!40000 ALTER TABLE `order_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `order_detail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` int(11) DEFAULT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `receiver_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `receiver_phone` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `receiver_email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `receiver_address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `total` double DEFAULT NULL,
  `status` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `orders`
--

LOCK TABLES `orders` WRITE;
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_category`
--

DROP TABLE IF EXISTS `product_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_category`
--

LOCK TABLES `product_category` WRITE;
/*!40000 ALTER TABLE `product_category` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_company`
--

DROP TABLE IF EXISTS `product_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_company` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company_short_name` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar_path` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_company`
--

LOCK TABLES `product_company` WRITE;
/*!40000 ALTER TABLE `product_company` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_image`
--

DROP TABLE IF EXISTS `product_image`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `product_image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image_path` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_image`
--

LOCK TABLES `product_image` WRITE;
/*!40000 ALTER TABLE `product_image` DISABLE KEYS */;
/*!40000 ALTER TABLE `product_image` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sku` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `stock` int(11) DEFAULT NULL,
  `product_company_id` int(11) DEFAULT NULL,
  `feature_image_path` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_description` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `long_description` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_recommended` tinyint(4) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'superadmin','Quản trị cấp cao','QT cap cao','2022-08-03 07:13:53','2022-08-21 12:48:20'),(2,'admin','Quản trị viên','Quan tri vienco quyen sau qt cao cap','2022-08-03 07:13:53','2022-08-03 07:13:53'),(3,'content','Người chỉnh sửa nội dung',NULL,'2022-08-03 07:14:37','2022-08-03 07:14:37'),(4,'designer','Nhà thiết kế',NULL,'2022-08-03 07:14:37','2022-08-03 07:14:37'),(5,'VIP_customer','Khách hàng VIP',NULL,'2022-12-01 13:31:26','2022-12-01 13:31:26'),(6,'customer','Khách hàng thường',NULL,'2022-12-01 13:32:38','2022-12-01 13:32:38');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sliders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `employee_id` int(11) NOT NULL,
  `title` text,
  `avatar_path` text,
  `short_description` text,
  `link` text,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sliders`
--

LOCK TABLES `sliders` WRITE;
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;
/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(250) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(11) NOT NULL,
  `phone` varchar(15) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avatar_path` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `display_name` varchar(1000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `salary` int(11) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `id_card_front` text COLLATE utf8mb4_unicode_ci,
  `id_card_back` text COLLATE utf8mb4_unicode_ci,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_user_role` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (0,'Chưa xác định','123456',1,'Chưa xác định',NULL,'Chưa xác định',0,'2022-12-14','Chưa xác định',0,0,NULL,NULL,'2022-12-07 15:23:15','2022-12-07 15:23:15',NULL),(1,'superadmin@gmail.com','$2a$12$Doy.V/zd9iRuXs2j.2UoB.6.HeqXymkqgjoOSd4emqx6O9xH7ZWQ6',1,'0123456789','https://image.thanhnien.vn/w1024/Uploaded/2022/ifyiy/2022_01_04/a5ee06311886d2d88b97-981.jpg','SuperAdmin',0,'2022-08-10','Số 10 tổ 36 Trung Kính, Cầu Giấy, Hà Nội',0,1,'https://minhnguyenhn.com/wp-content/uploads/2017/01/tao-anh-cmtnn-2.png','https://cms.luatvietnam.vn/uploaded/Images/Original/2019/10/25/tay-not-ruoi-xoa-seo-co-phai-lam-lai-chung-minh-nhan-dan_2510140542.jpg','2022-08-23 01:05:46','2022-12-09 10:49:41',NULL),(48,'admin@gmail.com','$2y$10$h7.2s6K9Ii7f3pmDt582xuXbEWOGm4OJEB/Z.vM896rSKV2VnTO8C',2,'01296987','https://blogcaodep.com/wp-content/uploads/2020/04/Side-Part-cho-khuon-mat-dai-mat-vuong-mat-trai-xoan.jpg','Admin1',0,'2022-12-16','Hà Nội, Việt Nam',90000,1,'https://minhnguyenhn.com/wp-content/uploads/2017/01/tao-anh-cmtnn-2.png','https://cms.luatvietnam.vn/uploaded/Images/Original/2019/10/25/tay-not-ruoi-xoa-seo-co-phai-lam-lai-chung-minh-nhan-dan_2510140542.jpg','2022-12-06 17:08:46','2022-12-11 02:52:00',NULL),(50,'khach1@gmail.com','$2y$10$Fv5grCHfo2Uhv2XsdGzmieXienxbhv6B/g1sp37oKAxuPOQwk3/mK',5,'0968655178','https://tocgiacaocap.com/upload/images/63c70f0264ac9cf2c5bd-3216(3).jpg','Khách 1',1,'2022-12-02','ádasd',NULL,1,'https://cms.luatvietnam.vn/uploaded/Images/Original/2019/10/25/tay-not-ruoi-xoa-seo-co-phai-lam-lai-chung-minh-nhan-dan_2510140542.jpg','https://cms.luatvietnam.vn/uploaded/Images/Original/2019/10/25/tay-not-ruoi-xoa-seo-co-phai-lam-lai-chung-minh-nhan-dan_2510140542.jpg','2022-12-06 17:23:04','2022-12-06 17:23:04',NULL),(51,'khach3@gmail.com','$2y$10$Fv5grCHfo2Uhv2XsdGzmieXienxbhv6B/g1sp37oKAxuPOQwk3/mK',6,'0259788589','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTm9YTjaQQ_dZnmodc5KP_TvaTqukQ2_Iq3HXGOOz4ci3MRg2NZIt7nNAoXrsZFfu_v_YQ&usqp=CAU','Khách 2',1,'2022-12-09','Hà Nội, Việt Nam',0,1,'https://cms.luatvietnam.vn/uploaded/Images/Original/2019/10/25/tay-not-ruoi-xoa-seo-co-phai-lam-lai-chung-minh-nhan-dan_2510140542.jpg','https://cms.luatvietnam.vn/uploaded/Images/Original/2019/10/25/tay-not-ruoi-xoa-seo-co-phai-lam-lai-chung-minh-nhan-dan_2510140542.jpg','2022-12-06 17:27:33','2022-12-11 02:56:58',NULL),(53,'khach2@gmail.com','$2y$10$7q1dSiu.yM2Po01L0.FNoOtLRhO5MKglEaxa7rAXjvzL6xzD4kgBS',6,'0123888999','https://mate.vn/images/cp_blog_post/12/goi-y-nhung-kieu-toc-dep-cho-mat-vuong-nang-khong-can-dung-dao-keo-mat-van-hoa-v-line.jpg','Khách 3',1,'2022-12-21','abc',0,1,'https://minhnguyenhn.com/wp-content/uploads/2017/01/tao-anh-cmtnn-2.png','https://cms.luatvietnam.vn/uploaded/Images/Original/2019/10/25/tay-not-ruoi-xoa-seo-co-phai-lam-lai-chung-minh-nhan-dan_2510140542.jpg','2022-12-10 04:42:49','2022-12-10 16:05:32',NULL),(54,'khach4@gmail.com','$2y$10$Bz5..kx.pi2n06yceyqlf.BJkXwaH29RE4FCwdmZgigxwU0AwvRHm',6,'09926254','https://lacongaithattuyet.vn/wp-content/uploads/2017/12/toc-ngan-xoan-don-song.jpg','Khách 4',1,'2022-12-15','Hà Nội, Việt Nam',0,1,'https://minhnguyenhn.com/wp-content/uploads/2017/01/tao-anh-cmtnn-2.png','https://cms.luatvietnam.vn/uploaded/Images/Original/2019/10/25/tay-not-ruoi-xoa-seo-co-phai-lam-lai-chung-minh-nhan-dan_2510140542.jpg','2022-12-11 03:01:19','2022-12-11 03:01:19',NULL),(55,'khach5@gmail.com','$2y$10$Rx4DQygOBVPBYHrZgwj0UObEozQBoW6XXGDg2dijB9ZbcloCw8N1K',6,'08458848','https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTY7r9GJc2YqNTTDhAdv1bP4sg0TjfQn1zzXmM-dCO-o9ogK_lR647pjZpulk71ApgVoh8&usqp=CAU','Khách 5',0,'2022-12-22','PTIT Hà Nội',0,1,'https://minhnguyenhn.com/wp-content/uploads/2017/01/tao-anh-cmtnn-2.png','https://cms.luatvietnam.vn/uploaded/Images/Original/2019/10/25/tay-not-ruoi-xoa-seo-co-phai-lam-lai-chung-minh-nhan-dan_2510140542.jpg','2022-12-11 03:01:59','2022-12-11 03:01:59',NULL),(56,'khach6@gmail.com','$2y$10$4.19me/Fp1YIWT4ypdDIyeNxnFFyxHYQ.C5TzFyDPZEyE3Jp5Yhqy',5,'0564595','https://www.tripnow.vn/wp-content/uploads/2017/10/kieu-toc-bob-xoan-mai-thua.jpg','Khách 6',1,'2022-12-14','Văn phòng nhà A1',0,1,'https://minhnguyenhn.com/wp-content/uploads/2017/01/tao-anh-cmtnn-2.png','https://cms.luatvietnam.vn/uploaded/Images/Original/2019/10/25/tay-not-ruoi-xoa-seo-co-phai-lam-lai-chung-minh-nhan-dan_2510140542.jpg','2022-12-11 03:02:57','2022-12-11 03:02:57',NULL),(57,'admin2@gmail.com','$2y$10$Tn4qWSpaVikIDze.IGnWKeyc/RjvNv6rpBrUOR/LbJnOgHeBSKKPC',2,'05156484844','https://image-us.eva.vn/upload/2-2021/images/2021-04-10/my-nhan-nhat-khoe-anh-mat-hinh-vuong-nghe-ly-do-vua-thuong-vua-buon-cuoi-ruriko-kojima_7-1618047982-298-width660height660.jpg','Admin 2',0,'2022-12-14','Hà Nội, Việt Nam',26556,1,'https://minhnguyenhn.com/wp-content/uploads/2017/01/tao-anh-cmtnn-2.png','https://cms.luatvietnam.vn/uploaded/Images/Original/2019/10/25/tay-not-ruoi-xoa-seo-co-phai-lam-lai-chung-minh-nhan-dan_2510140542.jpg','2022-12-11 03:25:42','2022-12-11 03:25:42',NULL);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'ewebapp'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-02-08 21:28:38
